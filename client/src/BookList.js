import Book from "./Book";

const BookList = (props)=>{
    const books = props.books.map(book => {
        return <Book book={book}
                     fetchBooks={()=>props.fetchBooks}
                     key={book.id}
        />
    })


    return(<div>{books}</div>)

}
export default BookList