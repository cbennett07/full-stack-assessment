import {Component} from "react";
import axios from "axios";

class BookCreator extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: '',
            title: '',
            author: '',
            favorite: false,
            errMessage: ''
        }
    }

    handleOnFavChange() {
        this.setState({favorite:!this.state.favorite});
    }
    handleOnAuthorChange(e) {
        this.setState({author:e.target.value});
    }
    handleOnTitleChange(e) {
        this.setState({title:e.target.value});
    }
    handleOnSubmit(){
        if (this.state.title.length>0 && this.state.author.length>0){
            this.state.errMessage="";
            const newBook = {
                title: this.state.title,
                author: this.state.author,
                favorite: this.state.favorite
            }
            const blankBook = {
                title: '',
                author: '',
                favorite: false
            }
            axios.post("http://localhost:3001/books",newBook)
                .then(console.log("finished delete"))
                .then(this.props.fetchBooks())
                .then(this.setState(blankBook),()=>{})
                .then(this.props.fetchBooks());
        } else{
            this.state.errMessage="Must enter text!";
            this.props.fetchBooks();
        }
    }
    render() {
        return (
            <div className="App">
                <table border={"1"} cellPadding={"5"}>
                    <tr>
                        <td>
                            <label htmlFor={"txtTitle" + this.state.id}>Book Title:</label>
                            <input type={"text"}
                                   value={this.state.title}
                                   id={"txtTitle" + this.state.id}
                                   onChange={(e) => this.handleOnTitleChange(e)}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label htmlFor={"txtAuthor" + this.state.id}>Book Author:</label>
                            <input type={"text"}
                                   value={this.state.author}
                                   id={"txtAuthor" + this.state.id}
                                   onChange={(e) => this.handleOnAuthorChange(e)}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label htmlFor={"txtFav" + this.state.id}>Favorite?</label>
                            <input type={"checkbox"}
                                   checked={this.state.favorite}
                                   id={"txtFav" + this.state.id}
                                   onChange={() => this.handleOnFavChange()}
                            />
                        </td>
                    </tr>
                    <tr>
                        <td rowSpan={"3"}>
                            <button onClick={() => this.handleOnSubmit()}
                                    onChange={()=> this.handleOnSubmit()}
                            >Add Book</button>
                            <label><font color="red">&nbsp;{this.state.errMessage}</font></label>
                        </td>
                    </tr>
                </table>
            </div>
        );
    }
}
export default BookCreator