import {Component} from "react";
import axios from "axios";

class Book extends Component {
    constructor(props){
        super(props);

        this.state={
            id: props.book.id,
            title: props.book.title,
            author: props.book.author,
            favorite: props.book.favorite
        }
    }


    handleOnFavChange() {
        this.setState({favorite:!this.state.favorite});
        const updateFav = {
            favorite: !this.state.favorite
        }
        axios.patch("http://localhost:3001/books/" + this.state.id,updateFav,()=>{})
            .then(console.log("finished update"))
            .then(this.props.fetchBooks());
    }


    handleOnDelete(){
        axios.delete("http://localhost:3001/books/" + this.state.id, ()=>{})
            .then(console.log("finished delete"))
            .then(this.props.fetchBooks());

    }

    render() {
        return (

            <div className="App">
                <ul>
                    <li style={ this.state.favorite ? { fontWeight:'bold'} : {}}>
                        {this.state.title}, by {this.state.author}&nbsp;&nbsp;
                        <button onClick={()=>this.handleOnFavChange()}>Favorite</button><space/>&nbsp;&nbsp;
                        <button onClick={()=>this.handleOnDelete()}>Remove</button>
                    </li>



                </ul>

            </div>
        );
    }

}
export default Book