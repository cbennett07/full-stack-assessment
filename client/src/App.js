import './App.css';
import {Component} from "react";
import BookList from "./BookList";
import BookCreator from "./BookCreator";
import axios from "axios";

class App extends Component {
    constructor(props){
        super(props);

        this.state={
            books: [],

        }
    }
    componentDidMount() {
        this.fetchBooks();
    }

    fetchBooks(){
        axios.get("http://localhost:3001/books")
            .then(response =>this.setState({books: response.data,}));
    }


    render() {
        return (
            <div>

                <div >
                    MY LIBRARY
                    <BookCreator fetchBooks={()=>this.fetchBooks()}/>
                </div>
                <div >

                    <BookList    books={this.state.books}
                                 fetchBooks={()=>this.fetchBooks()}/>
                </div>
            </div>
        );
    }
}

export default App;
