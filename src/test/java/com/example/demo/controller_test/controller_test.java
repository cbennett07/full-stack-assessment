package com.example.demo.controller_test;

import ch.qos.logback.classic.pattern.DateConverter;
import com.example.demo.model.Book;
import com.example.demo.repository.BookRepository;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc

public class controller_test {
    @Autowired
    BookRepository repository;

    @Autowired
    MockMvc mvc;


    @Test
    @Transactional
    @Rollback
    void createBookTest() throws Exception{
        MockHttpServletRequestBuilder request = post("/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"title\": \"my title\",\n" +
                        "    \"author\": \"Christopher Bennett\",\n" +
                        "    \"favorite\": true\n" +
                        "}");

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is("my title")))
                .andExpect(jsonPath("$.author", is("Christopher Bennett")))
                .andExpect(jsonPath("$.favorite",is(true)));

    }

    @Test
    @Transactional
    @Rollback
    void getBookTest() throws Exception {
        Book myBook = new Book();
        myBook.setTitle("My Title");
        myBook.setAuthor("My Author");
        myBook.setFavorite(false);
        this.repository.save(myBook);

        MockHttpServletRequestBuilder request = get("/books/" + myBook.getId())
                .contentType(MediaType.APPLICATION_JSON);

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is("My Title")))
                .andExpect(jsonPath("$.author", is("My Author")))
                .andExpect(jsonPath("$.favorite", is(false)));
    }

    @Test
    @Transactional
    @Rollback
    void updateBookTest() throws Exception {
        Book myBook = new Book();
        myBook.setTitle("My Title");
        myBook.setAuthor("My Author");
        myBook.setFavorite(false);
        this.repository.save(myBook);

        MockHttpServletRequestBuilder request = patch("/books/" + myBook.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"title\": \"My NEW Title\",\n" +
                        "    \"author\": \"My NEW Author\",\n" +
                        "    \"favorite\": true\n" +
                        "}");

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is("My NEW Title")))
                .andExpect(jsonPath("$.author", is("My NEW Author")))
                .andExpect(jsonPath("$.favorite", is(true)));
    }

    @Test
    @Transactional
    @Rollback
    void deleteBookTest() throws Exception {
        Book myBook = new Book();
        myBook.setTitle("My Title");
        myBook.setAuthor("My Author");
        myBook.setFavorite(false);
        this.repository.save(myBook);

        MockHttpServletRequestBuilder request = delete("/books/" + myBook.getId())
                .contentType(MediaType.APPLICATION_JSON);

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @Transactional
    @Rollback
    void listBooksTest() throws Exception {
        Book myBook = new Book();
        myBook.setTitle("My Title");
        myBook.setAuthor("My Author");
        myBook.setFavorite(false);
        this.repository.save(myBook);

        Book myBook2 = new Book();
        myBook2.setTitle("My Second Title");
        myBook2.setAuthor("My Second Author");
        myBook2.setFavorite(false);
        this.repository.save(myBook2);

        MockHttpServletRequestBuilder request = get("/books")
                .contentType(MediaType.APPLICATION_JSON);

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title", is("My Title")))
                .andExpect(jsonPath("$[1].title", is("My Second Title")));
    }
}
