package com.example.demo.controller;

import com.example.demo.model.Book;
import com.example.demo.repository.BookRepository;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
public class BookController {
    private final BookRepository repository;

    public BookController(BookRepository repository) {
        this.repository = repository;
    }

    //CREATE
    @PostMapping("/books")
    public Book addBook(@RequestBody Book newBook){
        return this.repository.save(newBook);
    }

    //READ
    @GetMapping("/books/{id}")
    public Book getBook(@PathVariable Long id){
        return this.repository.findById(id).get();
    }

    //UPDATE
    @PatchMapping("/books/{id}")
    public Book updateBook(@PathVariable Long id, @RequestBody Map<String, Object> newBook){
        Book oldBook = this.repository.findById(id).get();
        newBook.forEach((key, value)->{
            switch (key){
                case "title": oldBook.setTitle((String)value);break;
                case "author": oldBook.setAuthor((String)value);break;
                case "favorite":oldBook.setFavorite((Boolean)value);break;

            }
        });

        return this.repository.save(oldBook);
    }

    //DELETE
    @DeleteMapping("/books/{id}")
    public void deleteBook(@PathVariable Long id){
        this.repository.deleteById(id);
    }

    //LIST
    @GetMapping("/books")
    public Iterable<Book> listBooks (){
        return this.repository.findAll();
    }
}
